import { displayLog } from './utils';
import {Observable} from 'rxjs'

export default () => {
    /** start coding */
const hello = new Observable(function(observer){
    observer.next("Hello"),
    setTimeout(() => {
        observer.next("word")
    },2000);
    
})

const subscribe = hello.subscribe(evt =>displayLog(evt))
    /** end coding */
}